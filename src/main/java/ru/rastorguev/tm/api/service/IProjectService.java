package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllByUserId(String userId);

    void removeAllByUserId(String userId);

    String getProjectIdByNumber(int number);

    String getProjectIdByNumberForUser(int number, String userId);
}
