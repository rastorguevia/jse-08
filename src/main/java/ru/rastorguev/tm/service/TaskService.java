package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Override
    public IRepository<Task> getRepository() {
        return taskRepository;
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeTaskListByProjectId(projectId);
    }

    @Nullable
    @Override
    public List<Task> filterTaskListByProjectId(@Nullable final String projectId, @Nullable final Collection<Task> taskCollection) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskCollection == null) return null;
        return taskRepository.filterTaskListByProjectId(projectId, taskCollection);
    }

    @Nullable
    @Override
    public String getTaskIdByNumber(final int number, @Nullable final List<Task> filteredListOfTasks) {
        if (filteredListOfTasks == null) return null;
        for (@NotNull final Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }
}