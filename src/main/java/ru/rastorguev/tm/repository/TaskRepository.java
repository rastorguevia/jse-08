package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Task;

import java.util.*;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeTaskListByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(findAll());
        for (final Task task: listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                remove(task.getId());
            }
        }
    }

    @NotNull
    @Override
    public List<Task> filterTaskListByProjectId(@NotNull final String projectId, @NotNull final Collection<Task> taskCollection) {
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskCollection);
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (!listOfTasks.get(i).getProjectId().equals(projectId)) {
                listOfTasks.remove(i);
            }
        }
        return listOfTasks;
    }
}