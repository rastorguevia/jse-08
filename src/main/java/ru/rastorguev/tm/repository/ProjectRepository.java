package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;

import java.util.*;

@RequiredArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Override
    public Project remove(@NotNull final String projectId) {
        final List<Task> taskList = new LinkedList<>();
        taskList.addAll(taskRepository.findAll());
        for (final Task task: taskList) {
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
            }
        }
        return map.remove(projectId);
    }

    @Override
    public void removeAll() {
        map.clear();
        taskRepository.removeAll();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        @NotNull final List<Project> filteredListOfProjects = new LinkedList<>();
        for (@NotNull final Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                filteredListOfProjects.add(project);
            }
        }
        return filteredListOfProjects;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        for (@NotNull final Project project : listOfProjects) {
            if (project.getUserId().equals(userId)) {
                remove(project.getId());
            }
        }
    }

    @NotNull
    @Override
    public String getProjectIdByNumber(final int number) {
        @NotNull final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(findAll());
        return listOfProjects.get(number - 1).getId();
    }
}