package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Project clear");
        @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getProjectService().removeAllByUserId(userId);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role [] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}