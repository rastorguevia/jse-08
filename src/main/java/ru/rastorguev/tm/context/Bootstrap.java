package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.view.View.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.command.system.AboutCommand;
import ru.rastorguev.tm.command.system.ExitCommand;
import ru.rastorguev.tm.command.system.HelpCommand;
import ru.rastorguev.tm.command.project.*;
import ru.rastorguev.tm.command.task.*;
import ru.rastorguev.tm.command.user.*;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.service.*;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandMap);

    public void init(@NotNull final Class[] classes) throws Exception {
        initCommands(classes);
        createDefaultUsers();
        launchConsole();
    }

    private void initCommands(@NotNull final Class[] classes) throws Exception {
        for (@NotNull Class commandClass : classes) {
            if (AbstractCommand.class.isAssignableFrom(commandClass)) {
                @NotNull final AbstractCommand command = (AbstractCommand) commandClass.newInstance();
                command.setServiceLocator(this);
                commandMap.put(command.getName(), command);
            }
        }
    }

    public void launchConsole() throws Exception {
        showWelcomeMsg();

        @Nullable String input = "";
        while (true) {
            input = terminalService.nextLine().toLowerCase();
            execute(input);
        }
    }

    private void execute(@Nullable final String input) throws Exception {
        if (input == null || input.isEmpty()) {
            showUnknownCommandMsg();
            return;
        }
        @Nullable final AbstractCommand command = commandMap.get(input);
        if (command == null) {
            showUnknownCommandMsg();
            return;
        }
        @NotNull final boolean secureCheck = !command.secure() || (command.secure() && userService.isAuth());
        @NotNull final boolean roleCheck = command.roles() == null ||
                (command.roles() != null && userService.isRolesAllowed(command.roles()));
        if (secureCheck && roleCheck) command.execute();
        else showAccessDeniedMsg();
    }

    private void createDefaultUsers() {
        userService.userRegistry("user", "user123");
        userService.adminRegistry("admin", "admin123");
    }
}