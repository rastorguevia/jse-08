package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;


@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @NotNull
    private String projectId = "";

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    public Task(final @Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        this.projectId = projectId;
    }

}