package ru.rastorguev.tm.view;

import static ru.rastorguev.tm.util.DateUtil.*;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class View {

    public static void showWelcomeMsg() {
        System.out.println("* TASK MANAGER * \n" +
                "* help - show all commands * \n" +
                "* command case is not important *");
    }

    public static void showUnknownCommandMsg() {
        System.out.println("Unknown Command \n" +
                "try again");
    }

    public static void showAccessDeniedMsg() {
        System.out.println("Access Denied");
    }

    public static void printAllProjects(@Nullable final Collection<Project> projectCollection) {
        if (projectCollection == null) return;
        @NotNull final List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectCollection);
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public static void printAllProjectsForUser(@Nullable final List<Project> listOfProjects) {
        if (listOfProjects == null) return;
        for (int i = 0; i < listOfProjects.size(); i++) {
            System.out.println((i+1) + "." + listOfProjects.get(i).getName());
        }
    }

    public static void printProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("Project name: " + project.getName() +
                "\nProject description: " + project.getDescription() +
                "\nProject start date: " + dateFormatter.format(project.getStartDate()) +
                "\nProject end date: " + dateFormatter.format(project.getEndDate()));
    }

    public static void printTaskListByProjectId(@Nullable final String projectId,@Nullable final Collection<Task> taskCollection) {
        if (projectId == null || taskCollection == null) return;
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        @NotNull final List<Task> filteredListOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskCollection);
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (listOfTasks.get(i).getProjectId().equals(projectId)) {
                filteredListOfTasks.add(listOfTasks.get(i));
            }
        }
        for (int i = 0; i < filteredListOfTasks.size(); i++) {
            System.out.println((i + 1) + "." + filteredListOfTasks.get(i).getName());
        }
    }

    public static void printTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("Task name: " + task.getName() +
                "\nTask description: " + task.getDescription() +
                "\nTask start date: " + dateFormatter.format(task.getStartDate()) +
                "\nTask end date: " + dateFormatter.format(task.getEndDate()));
    }

    public static void printUserProfile(@Nullable final User user) {
        if (user == null) return;
        System.out.println("User login: " + user.getLogin() +
                "\nUser role: " + user.getRole().getDisplayName());
    }
}