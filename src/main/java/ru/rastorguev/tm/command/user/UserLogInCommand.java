package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

@NoArgsConstructor
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "log_in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Log in");
        System.out.println("Enter login");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().logIn(login, password);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }
}
