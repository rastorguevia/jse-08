package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task edit");
        System.out.println("Enter Project ID");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printAllProjectsForUser(serviceLocator.getProjectService().findAllByUserId(user.getId()));
        final int projectNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String projectId = serviceLocator.getProjectService().getProjectIdByNumberForUser(projectNumber, user.getId());
        @Nullable final Collection<Task> taskCollection = serviceLocator.getTaskService().findAll();
        printTaskListByProjectId(projectId, taskCollection);
        @Nullable final List<Task> filteredTaskList = serviceLocator.getTaskService().filterTaskListByProjectId(projectId, taskCollection);
        System.out.println("Enter Task ID");
        final int taskNumber = Integer.parseInt(serviceLocator.getTerminalService().nextLine());
        @Nullable final String taskId = serviceLocator.getTaskService().getTaskIdByNumber(taskNumber ,filteredTaskList);
        @Nullable final Task task = serviceLocator.getTaskService().findOne(taskId);
        @Nullable final Task editedTask = new Task(task.getProjectId());
        editedTask.setId(task.getId());
        System.out.println("Edit name? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter task name");
            editedTask.setName(serviceLocator.getTerminalService().nextLine());
        } else editedTask.setName(task.getName());
        System.out.println("Edit description? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedTask.setDescription(serviceLocator.getTerminalService().nextLine());
        } else editedTask.setDescription(task.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedTask.setStartDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
        } else editedTask.setStartDate(task.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedTask.setEndDate(stringToDate(serviceLocator.getTerminalService().nextLine()));
        } else editedTask.setEndDate(task.getEndDate());
        serviceLocator.getTaskService().merge(editedTask);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}