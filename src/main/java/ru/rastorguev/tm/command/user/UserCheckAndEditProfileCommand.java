package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class UserCheckAndEditProfileCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "check_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Check and edit current profile.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Check and edit");
        @NotNull final User user = serviceLocator.getUserService().getCurrentUser();
        printUserProfile(user);
        System.out.println("Do you want to edit? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
            System.out.println("Enter new login");
            @NotNull final String login = serviceLocator.getTerminalService().nextLine();
            if (serviceLocator.getUserService().isExistByLogin(login)) {
                System.out.println("This login already exist");
            } else user.setLogin(login);
            serviceLocator.getUserService().merge(user);
            System.out.println("OK");
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}