package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void removeTaskListByProjectId(String projectId);

    List<Task> filterTaskListByProjectId(String projectId, Collection<Task> taskCollection);

    String getTaskIdByNumber(int number, List<Task> filteredListOfTasks);
}
