package ru.rastorguev.tm;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.command.system.*;
import ru.rastorguev.tm.command.project.*;
import ru.rastorguev.tm.command.task.*;
import ru.rastorguev.tm.command.user.*;
import ru.rastorguev.tm.context.Bootstrap;

public final class Application {

    @NotNull
    private final static Class[] CLASSES = {
            AboutCommand.class, ExitCommand.class, HelpCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectEditCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class, ProjectSelectCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class, TaskEditCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class, TaskSelectCommand.class,

            UserAdminRegistryCommand.class, UserCheckAndEditProfileCommand.class, UserLogInCommand.class,
            UserLogOutCommand.class, UserPasswordUpdateCommand.class, UserUserRegistryCommand.class
    };

    public static void main(final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }
}