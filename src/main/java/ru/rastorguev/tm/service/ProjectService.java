package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IProjectService;
import ru.rastorguev.tm.entity.Project;

import static ru.rastorguev.tm.util.ArrayUtil.*;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    @Override
    public IRepository<Project> getRepository() {
        return projectRepository;
    }

    @Nullable
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public String getProjectIdByNumber(final int number) {
        return projectRepository.getProjectIdByNumber(number);
    }

    @Nullable
    @Override
    public String getProjectIdByNumberForUser(final int number, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final List<Project> filteredListOfProjects = findAllByUserId(userId);
        if (isIndexExist(filteredListOfProjects, number)) return null;
        return filteredListOfProjects.get(number - 1).getId();
    }
}