package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.Collection;

public interface IRepository<E extends AbstractEntity> {

    Collection<E> findAll();

    E findOne(String entityId);

    E persist(E entity);

    E merge(E entity);

    E remove(String entityId);

    void removeAll();
}
