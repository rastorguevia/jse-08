package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.enumerated.Role;

import java.io.IOException;

@NoArgsConstructor
public final class UserUserRegistryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "user_registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry new User.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("User registry");
        System.out.println("Enter login");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        System.out.println("Enter password");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().userRegistry(login, password);
        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }
}